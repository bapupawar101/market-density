﻿using System.Web;
using System.Web.Optimization;

namespace MarketDensity_Web_Application
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", "~/Scripts/bootstrap.js", "~/Scripts/jquery_validate.js", "~/Scripts/site.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.css", "~/Content/site.css", "~/Content/font-awesome.css"));
        }
    }
}