﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MarketDensity_Web_Application
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            ////New Route
            //routes.MapRoute(
            //   name: "login",
            //   url:"{controller}/{action}/{id}",
            //   defaults: new { controller = "Account", action = "login" }
            //);

            //routes.MapRoute("specificRoute", "{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional });

            //routes.MapRoute("Root", "{action}/{id}", new { action = "login", id = UrlParameter.Optional });
            //enabling attribute routing
            //routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}