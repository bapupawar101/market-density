﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MarketDensity_Web_Application.Models
{
    public class MarketDensity_Db_Contetxt : DbContext
    {
        public MarketDensity_Db_Contetxt()
            : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MarketDensity_Db_Contetxt, MarketDensity_Web_Application.Migrations.Configuration>());
        }

        public DbSet<User> tbl_user { get; set; }
        public DbSet<Contact> tbl_contactus { get; set; }
        public DbSet<Report_Master> tbl_report { get; set; }
        public DbSet<Batch_Master> tbl_batch_master { get; set; }
        public DbSet<Report_ErrorLog> tbl_report_error_log { get; set; }
        public DbSet<Publisher> tbl_publisher { get; set; }
        public DbSet<Category> tbl_category { get; set; }
        public DbSet<Cart> tbl_cart { get; set; }
        public DbSet<OrderMaster> tbl_order_master { get; set; }
        public DbSet<OrderInfo> tbl_order_info { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

    public class Report_Master
    {
        public int id { get; set; }
        public string code { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string toc { get; set; }
        public string list_of_tables { get; set; }
        public string list_of_figures { get; set; }
        public string publisher { get; set; }
        public string publish_date { get; set; }
        public int no_of_pages { get; set; }
        public string category { get; set; }
        public string sub_category { get; set; }
        public int price_single_user { get; set; }
        public int price_online_only { get; set; }
        public int price_data_pack { get; set; }
        public int price_multi_user { get; set; }
        public int price_enterprise_user { get; set; }
        public string country { get; set; }
        public string region { get; set; }
        public string meta_keywords { get; set; }
        public string meta_description { get; set; }

        public int batch_id { get; set; }
        [ForeignKey("batch_id")]
        public virtual Batch_Master batch { get; set; }
    }

    public class Batch_Master
    {
        public int id { get; set; }
        public DateTime uploaded_date { get; set; }
        public string file_name { get; set; }
        public string uploaded_by { get; set; }
        public Batch_Master()
        {
            uploaded_date = DateTime.Now;
            uploaded_by = "Admin";
        }
    }

    public class Report_ErrorLog
    {
        public int id { get; set; }
        public int batch_id { get; set; }
        public string error_message { get; set; }
        public int report_position { get; set; }
        public string report_title { get; set; }

        [ForeignKey("batch_id")]
        public virtual Batch_Master batch { get; set; }
    }

    public class Report
    {
        public string name { get; set; }
        public string code { get; set; }
        public string industry { get; set; }
        public string published_on { get; set; }
        public string report_highlights { get; set; }
        public string research_methodology { get; set; }
        public string build_your_toc { get; set; }
        public List<Item> toc { get; set; }
        public List<string> RelevantReports { get; set; }
        public string dirName { get; set; }
        public Report()
        {
            toc = new List<Item>();
            RelevantReports = new List<string>();
        }
    }

    public class Item
    {
        public string name { get; set; }
        public List<Item> items { get; set; }
        public Item()
        {
            items = new List<Item>();
        }
    }

    public class Pager
    {
        public Pager(int totalItems, int? page, int pageSize = 10)
        {
            // calculate total, start and end pages
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize);
            var currentPage = page != null ? (int)page : 1;
            var startPage = currentPage - 5;
            var endPage = currentPage + 4;
            if (startPage <= 0)
            {
                endPage -= (startPage - 1);
                startPage = 1;
            }
            if (endPage > totalPages)
            {
                endPage = totalPages;
                if (endPage > 10)
                {
                    startPage = endPage - 9;
                }
            }

            TotalItems = totalItems;
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalPages = totalPages;
            StartPage = startPage;
            EndPage = endPage;
        }

        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int TotalPages { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
    }

    public class User
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string companyName { get; set; }
        public string phoneNumber { get; set; }
        public string password { get; set; }
        public DateTime reg_Date { get; set; }
        public bool active { get; set; }
        public Role role { get; set; }
        public User()
        {
            reg_Date = DateTime.Now;
            active = true;
        }
    }

    public class Contact
    {
        public int id { get; set; }
        public string name { get; set; }
        public string companyName { get; set; }
        public string phoneNumber { get; set; }
        public string comments { get; set; }
        public string reportCode { get; set; }
        public string email { get; set; }
        public string request_category { get; set; }
        public DateTime contact_date { get; set; }
        public Contact()
        {
            contact_date = DateTime.Now;
        }
    }

    public class Publisher
    {
        public int id { get; set; }
        public string displayName { get; set; }
        public string name { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public bool active { get; set; }
        public string pub_contact_name { get; set; }
        public string pub_contact_email { get; set; }
        public string phone { get; set; }
        public string addr { get; set; }
        public Publisher()
        {
            DateTime now = DateTime.Now;
            startDate = now;
            endDate = now;
            active = true;
        }
    }

    public class Cart
    {
        public int id { get; set; }
        public int userId { get; set; }
        public string reportId { get; set; }
        public DateTime datetime { get; set; }
        public LicenseType licenseType { get; set; }
        public Cart()
        {
            datetime = DateTime.Now;
        }

        public Report_Master report { get; set; }
    }

    public enum LicenseType
    {
        single = 1,
        multi,
        enterprise,
        online_only,
        data_pack
    }

    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public byte[] icon { get; set; }
    }

    public class OrderMaster
    {
        public int id { get; set; }
        public string order_id { get; set; }
        public string total { get; set; }
        public string tax { get; set; }
        public string sub_total { get; set; }
        public string invoice_number { get; set; }
        public int userId { get; set; }
        public bool success { get; set; }
        public string token { get; set; }
        public string payment_method { get; set; }
        public string paymentId { get; set; }
        public string PayerID { get; set; }
        public DateTime order_date { get; set; }

        public OrderMaster()
        {
            order_date = DateTime.Now;
        }

        [ForeignKey("userId")]
        public virtual User user { get; set; }
    }

    public class OrderInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public string price { get; set; }
        public string currency { get; set; }
        public int quantity { get; set; }
        public string sku { get; set; }
        public int order_master_id { get; set; }

        [ForeignKey("order_master_id")]
        public OrderMaster order_master { get; set; }
    }

    public enum RequestCategory
    {
        Enquiry = 1,
        Sample_and_Brochure,
        Request_Customization,
        Discount
    }

    public enum Role
    {
        customer = 1,
        admin = 2
    }
}