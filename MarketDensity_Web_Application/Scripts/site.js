﻿function BSAlert(title, message, type) {
    var _class = "success";
    if (type == "w" || type == "e")
        _class = "danger";
    $("#BSPopup .modal-title").html("<span class='" + _class + "'>" + title + "</span>");
    $("#BSPopup .modal-body").html(message);
    $("#BSPopup").modal("show");
}