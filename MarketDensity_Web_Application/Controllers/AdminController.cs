﻿using MarketDensity_Web_Application.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarketDensity_Web_Application.Controllers
{
    [Authorization]
    public class AdminController : Controller
    {
        MarketDensity_Db_Contetxt db = new MarketDensity_Db_Contetxt();

        public ActionResult Dashboard()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult index(string username, string password)
        {
            if (username == "admin")
            {

                if (password == "123")
                {
                    Session["user"] = username;
                    return RedirectToAction("Dashboard");
                }
                else
                    TempData["errorMsg"] = "Wrong Password";
            }
            else
            {
                TempData["errorMsg"] = "Wrong Username";
            }
            return View();
        }

        public ActionResult ShowAllBatches()
        {
            var list_of_batches = db.tbl_batch_master.ToList();
            return View(list_of_batches);
        }

        public ActionResult ReportErrorLog(int batchId)
        {
            var listOfErrorLogs = db.tbl_report_error_log.Where(p => p.batch_id == batchId).ToList();
            return View(listOfErrorLogs);
        }

        public ActionResult deletebatch(int batchId)
        {
            var toc = db.tbl_batch_master.Find(batchId);
            db.tbl_batch_master.Remove(toc);
            db.SaveChanges();
            return RedirectToAction("ShowAllBatches", "admin");
        }

        public ActionResult ShowAllReports(int pageIndex = 0, int pageSize = 100)
        {
            var listOfTOC = (from toc in db.tbl_report
                             select new
                             {
                                 code = toc.code,
                                 title = toc.title,
                                 id = toc.id,
                                 publisher = toc.publisher,
                                 category = toc.category,
                                 country = toc.country
                             }).ToList();

            var list = new List<Report_Master>();
            foreach (var s in listOfTOC)
            {
                list.Add(new Report_Master { id = s.id, title = s.title, code = s.code, publisher = s.publisher,country=s.country,category=s.category });
            }
            return View(list);
        }

        public ActionResult upload_reports()
        {
            return View();
        }

        [HttpPost]
        public ActionResult upload_reports(HttpPostedFileBase file)
        {
            int title = Convert.ToInt32(Request.Form["title"]);
            int description = Convert.ToInt32(Request.Form["description"]);
            int TOC = Convert.ToInt32(Request.Form["toc"]);
            int list_of_tables = Convert.ToInt32(Request.Form["list_of_tables"]);
            int list_of_figures = Convert.ToInt32(Request.Form["list_of_figures"]);
            int publisher = Convert.ToInt32(Request.Form["publisher"]);
            int publish_date = Convert.ToInt32(Request.Form["publish_date"]);
            int no_of_pages = Convert.ToInt32(Request.Form["no_of_pages"]);

            int category = Convert.ToInt32(Request.Form["category"]);
            int sub_category = Convert.ToInt32(Request.Form["sub_category"]);
            int price_single_user = Convert.ToInt32(Request.Form["price_single_user"]);
            int price_multi_user = Convert.ToInt32(Request.Form["price_multi_user"]);
            int price_enterprise_user = Convert.ToInt32(Request.Form["price_enterprise_user"]);
            int country = Convert.ToInt32(Request.Form["country"]);
            int region = Convert.ToInt32(Request.Form["region"]);
            int meta_keywords = Convert.ToInt32(Request.Form["meta_keywords"]);

            int meta_description = Convert.ToInt32(Request.Form["meta_description"]);

            //-----Creating batch object----
            Batch_Master batch_obj = new Batch_Master();
            batch_obj.file_name = file.FileName;
            db.tbl_batch_master.Add(batch_obj);
            db.SaveChanges();
            //-----End----------------------

            List<Report_Master> tocs = new List<Report_Master>();
            using (var package = new ExcelPackage(file.InputStream))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                for (int row = 2; worksheet.Cells[row, 1].Value != null; row++)
                {
                    Report_Master toc = new Report_Master();
                    toc.title = worksheet.Cells[row, title].Value != null ? worksheet.Cells[row, title].Value.ToString() : "";
                    toc.description = worksheet.Cells[row, description].Value != null ? worksheet.Cells[row, description].Value.ToString() : "";
                    toc.toc = worksheet.Cells[row, TOC].Value != null ? worksheet.Cells[row, TOC].Value.ToString() : "";
                    toc.list_of_tables = worksheet.Cells[row, list_of_tables].Value != null ? worksheet.Cells[row, list_of_tables].Value.ToString() : "";
                    toc.list_of_figures = worksheet.Cells[row, list_of_tables].Value != null ? worksheet.Cells[row, list_of_figures].Value.ToString() : "";
                    toc.publisher = worksheet.Cells[row, publisher].Value != null ? worksheet.Cells[row, publisher].Value.ToString() : "";
                    toc.publish_date = worksheet.Cells[row, publish_date].Value != null ? worksheet.Cells[row, publish_date].Value.ToString() : "";
                    toc.no_of_pages = worksheet.Cells[row, no_of_pages].Value != null ? Convert.ToInt32(worksheet.Cells[row, no_of_pages].Value.ToString()) : 0;
                    toc.category = worksheet.Cells[row, category].Value != null ? worksheet.Cells[row, category].Value.ToString() : "";
                    toc.sub_category = worksheet.Cells[row, sub_category].Value != null ? worksheet.Cells[row, sub_category].Value.ToString() : "";
                    toc.price_single_user = worksheet.Cells[row, price_single_user].Value != null ? Convert.ToInt32(worksheet.Cells[row, price_single_user].Value.ToString()) : 0;
                    toc.price_multi_user = worksheet.Cells[row, price_multi_user].Value != null ? Convert.ToInt32(worksheet.Cells[row, price_multi_user].Value.ToString()) : 0;
                    toc.price_enterprise_user = worksheet.Cells[row, price_enterprise_user].Value != null ? Convert.ToInt32(worksheet.Cells[row, price_enterprise_user].Value.ToString()) : 0;
                    toc.country = worksheet.Cells[row, country].Value != null ? worksheet.Cells[row, country].Value.ToString() : "";
                    toc.region = worksheet.Cells[row, region].Value != null ? worksheet.Cells[row, region].Value.ToString() : "";
                    toc.meta_keywords = worksheet.Cells[row, meta_keywords].Value != null ? worksheet.Cells[row, meta_keywords].Value.ToString() : "";
                    toc.meta_description = worksheet.Cells[row, meta_description].Value != null ? worksheet.Cells[row, meta_description].Value.ToString() : "";
                    toc.batch_id = batch_obj.id;

                    if (string.IsNullOrEmpty(toc.category))
                    {
                        Report_ErrorLog log = new Report_ErrorLog();
                        log.batch_id = batch_obj.id;
                        log.report_position = row;
                        log.report_title = toc.title;
                        log.error_message = "Report does not have category.";
                        db.tbl_report_error_log.Add(log);
                    }
                    else
                    {
                        db.tbl_report.Add(toc);
                        tocs.Add(toc);
                    }
                }
            }
            db.SaveChanges();
            foreach (var toc in tocs)
            {
                toc.code = "MD" + toc.category.Substring(0, 2).ToUpper() + toc.id;
                db.tbl_report.Attach(toc);
                db.Entry(toc).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("ShowAllReports", "admin");
        }

        public ActionResult delete_report(int id)
        {
            var toc = db.tbl_report.Find(id);
            db.tbl_report.Remove(toc);
            db.SaveChanges();
            return RedirectToAction("ShowAllReports", "admin");
        }

        //---------Manage Publishers--------------
        public ActionResult managepublishers()
        {
            var publishers = db.tbl_publisher.OrderByDescending(p => p.startDate).ToList();
            return View(publishers);
        }

        public ActionResult addnewpublisher()
        {
            return View();
        }

        [HttpPost]
        public ActionResult addorupdatepublisher(Publisher record)
        {
            db.tbl_publisher.Add(record);
            db.SaveChanges();
            return RedirectToAction("managepublishers", "admin");
        }

        public ActionResult deletepublisher(int id)
        {
            var dbUser = db.tbl_publisher.Find(id);
            db.tbl_publisher.Remove(dbUser);
            db.SaveChanges();
            return RedirectToAction("managepublishers", "admin");
        }
        //------------End Manage Publisher-----------------------

        //------------Manage Categories--------------------------
        public ActionResult managecategories()
        {
            var categories = db.tbl_category.ToList();
            return View(categories);
        }

        public ActionResult AddOrUpdateCategories(HttpPostedFileBase file,Category record)
        {
            if (file != null)
            {
                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                record.icon = data;
            }

            db.tbl_category.Add(record);
            db.SaveChanges();
            return RedirectToAction("managecategories", "admin");
        }

        public ActionResult deletecategory(int id)
        {
            var dbUser = db.tbl_category.Find(id);
            db.tbl_category.Remove(dbUser);
            db.SaveChanges();
            return RedirectToAction("managecategories", "admin");
        }
        //---------------End Manage Categories---------------------
        //---------------Manage Orders-----------------------------
        public ActionResult manageorders()
        {
            var orders = db.tbl_order_master.OrderByDescending(p=>p.order_date).ToList();
            return View(orders);
        }

        public ActionResult orderdetails(int id)
        {
            var orderMaster = db.tbl_order_master.Find(id);
            var orders_info = db.tbl_order_info.Where(p=>p.order_master_id==id).ToList();
            ViewBag.ordermaster = orderMaster;
            return View(orders_info);
        }
        //---------------End---------------------------------------
    }
}
