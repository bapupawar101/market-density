﻿using AttributeRouting.Web.Mvc;
using MarketDensity_Web_Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarketDensity_Web_Application.Controllers
{
    [Authorization]
    public class UserController : Controller
    {
        MarketDensity_Db_Contetxt db = new MarketDensity_Db_Contetxt();
        public User GetUser()
        {
            return Session["user"] as User;
        }

        public ActionResult dashboard()
        {
            return View();
        }

        [Route("{action=orderhistory}")]
        public ActionResult orderhistory()
        {
            var user = GetUser();
            var orders = db.tbl_order_master.Where(p => p.userId == user.id).OrderByDescending(p => p.order_date).ToList();
            return View(orders);
        }

        [Route("{action=orderindetail}")]
        public ActionResult orderindetail(int id)
        {
            var orderMaster = db.tbl_order_master.Find(id);
            var orders_info = db.tbl_order_info.Where(p => p.order_master_id == id).ToList();
            ViewBag.ordermaster = orderMaster;
            return View(orders_info);
        }

        [Route("{action=changepassword}")]
        public ActionResult changepassword()
        {
            return View();
        }

        [Route("changepassword-post")]
        [HttpPost]
        public ActionResult changepassword_post(string password)
        {
            var user = GetUser();
            var dbuser = db.tbl_user.Find(user.id);
            dbuser.password = password;
            db.SaveChanges();
            Session.Abandon();
            TempData["msg"] = "Password has been changed! please login again.";
            return RedirectToAction("login", "account");
        }
    }
}
