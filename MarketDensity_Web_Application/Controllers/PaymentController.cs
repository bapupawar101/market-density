﻿using AttributeRouting.Web.Mvc;
using MarketDensity_Web_Application.Helper;
using MarketDensity_Web_Application.Models;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarketDensity_Web_Application.Controllers
{
    [Authorization]
    public class PaymentController : Controller
    {
        MarketDensity_Db_Contetxt db = new MarketDensity_Db_Contetxt();
        [Route("{action=checkout}")]
        public ActionResult Checkout()
        {
            var user = GetUser();
            List<Cart> carts = db.tbl_cart.Where(p => p.userId == user.id).ToList();
            foreach (var item in carts)
            {
                item.report = db.tbl_report.Where(p => p.code == item.reportId).FirstOrDefault();
            }
            return View(carts);
        }

        public void EmptyCart()
        {
            var user = GetUser();
            List<Cart> carts = db.tbl_cart.Where(p => p.userId == user.id).ToList();
            foreach (var record in carts)
            {
                db.tbl_cart.Remove(record);
            }
            Session["shoppingCartCount"] = 0;
           db.SaveChanges();
        }

        public User GetUser()
        {
            return Session["user"] as User;
        }

        [Route("{action=paymentWithPaypal}")]
        public ActionResult PaymentWithPaypal(string Cancel = null)
        {
            //getting the apiContext  
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                //A resource representing a Payer that funds a payment Payment Method as paypal  
                //Payer Id will be returned when payment proceeds or click to pay  
                string payerId = Request.Params["PayerID"];
                string paymentId = Request.Params["paymentId"];
                if (string.IsNullOrEmpty(payerId))
                {
                    var cancel = Request.Params["Cancel"];
                    if (cancel == "true")
                        return View("FailureView");
                    //this section will be executed first because PayerID doesn't exist  
                    //it is returned by the create function call of the payment class  
                    // Creating a payment  
                    // baseURL is the url on which paypal sendsback the data.  
                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/payment/PaymentWithPayPal?";
                    //here we are generating guid for storing the paymentID received in session  
                    //which will be used in the payment execution  
                    var guid = Convert.ToString((new Random()).Next(100000));
                    //CreatePayment function gives us the payment approval url  
                    //on which payer is redirected for paypal account payment  
                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid, guid);
                    //get links returned from paypal in response to Create function call  
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            //saving the payapalredirect URL to which user will be redirected for payment  
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    // saving the paymentID in the key guid  
                    Session.Add(guid, createdPayment.id);
                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var token = Request.Params["token"];
                    //------------Change order status----------
                    var order = db.tbl_order_master.Where(p => p.order_id.Equals(guid)).FirstOrDefault();
                    if (order != null)
                    {
                        order.success = true;
                        order.token = token;
                        order.PayerID = payerId;
                        order.paymentId = paymentId;
                        db.SaveChanges();
                    }

                    EmptyCart();
                    //------------End--------------------------
                    // This function exectues after receving all parameters for the payment  

                    var executedPayment = ExecutePayment(apiContext, payerId, paymentId);
                    //If executed payment failed then we will show payment failure message to user  
                    if (executedPayment.state.ToLower() != "approved")
                    {
                        return View("FailureView");
                    }
                }
            }
            catch (Exception ex)
            {
                return View("FailureView");
            }
            //on successful payment, show success page to user.  
            return View("SuccessView");
        }
        private PayPal.Api.Payment payment;
        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new Payment()
            {
                id = paymentId
            };
            return this.payment.Execute(apiContext, paymentExecution);
        }
        private Payment CreatePayment(APIContext apiContext, string redirectUrl, string orderId)
        {
            //create itemlist and add item objects to it  
            var itemList = new ItemList()
            {
                items = new List<PayPal.Api.Item>()
            };

            var user = GetUser();
            List<Cart> carts = db.tbl_cart.Where(p => p.userId == user.id).ToList();
            foreach (var item in carts)
            {
                item.report = db.tbl_report.Where(p => p.code == item.reportId).FirstOrDefault();
            }

            foreach (var cartItem in carts)
            {
                var item = new PayPal.Api.Item();
                item.name = "Report id: " + cartItem.reportId + " (" + CommonUtils.GetLicenseTypeName(cartItem.licenseType) + ")";
                item.price = CommonUtils.GetReportPrice(cartItem.report, cartItem.licenseType).ToString();
                item.currency = "USD";
                item.quantity = "1";
                item.sku = cartItem.reportId;
                itemList.items.Add(item);
            }

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object  
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            var totalAmount = itemList.items.Sum(p => Convert.ToDouble(p.price));
            var tax = (totalAmount * Convert.ToDouble(ConfigurationManager.AppSettings["CA_Sales_Tax"]) / 100);
            // Adding Tax, shipping and Subtotal details  
            var details = new Details()
            {
                tax = tax.ToString("0.##"),
                subtotal = totalAmount.ToString(),
                shipping = "0"
            };
            //Final amount with details  
            var amount = new Amount()
            {
                currency = "USD",
                total = (totalAmount + tax).ToString("0.##"), // Total must be equal to sum of tax, shipping and subtotal.  
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction  
            string invoice_number = Convert.ToString((new Random()).Next(100000));
            transactionList.Add(new Transaction()
            {
                description = "Order Summary",
                invoice_number = invoice_number, //Generate an Invoice No  
                amount = amount,
                item_list = itemList
            });
            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext  

            //------Stoaring order details----
            OrderMaster om = new OrderMaster()
            {
                invoice_number = invoice_number,
                order_id = orderId,
                sub_total = (totalAmount + tax).ToString("0.##"),
                tax = tax.ToString("0.##"),
                total = totalAmount.ToString(),
                userId = user.id,
                payment_method = payer.payment_method
            };
            db.tbl_order_master.Add(om);
            db.SaveChanges();
            foreach (var it in itemList.items)
            {
                OrderInfo oi = new OrderInfo()
                {
                    name = it.name,
                    order_master_id = om.id,
                    price = it.price,
                    quantity = Convert.ToInt32(it.quantity),
                    currency = it.currency,
                    sku = it.sku
                };

                db.tbl_order_info.Add(oi);
            }
            db.SaveChanges();
            //------end-----------------------

            return this.payment.Create(apiContext);
        }
    }
}
