﻿using AttributeRouting.Web.Mvc;
using MarketDensity_Web_Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarketDensity_Web_Application.Controllers
{
    public class AccountController : Controller
    {
        MarketDensity_Db_Contetxt db = new MarketDensity_Db_Contetxt();
        [Route("{action=login}")]
        public ActionResult login(string returnUrl = "")
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [Route("/login-post")]
        [HttpPost]
        public ActionResult login_post(User user, string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            var dbUser = db.tbl_user.Where(p => p.email.ToLower().Equals(user.email.ToLower())).FirstOrDefault();
            if (dbUser != null)
            {
                if (dbUser.password.Equals(user.password))
                {
                    Session["user"] = dbUser;
                    Session["shoppingCartCount"] = new HomeController().GetShoppingCartItemsCount(dbUser);
                    if (string.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("dashboard", "user");
                    else
                        return Redirect(returnUrl);
                }
                else { ViewBag.msg = "Invalid Password"; }
            }
            else
                ViewBag.msg = "Email ID is not registered with us.";
            return View("login");
        }

        public ActionResult signout()
        {
            Session.Abandon();
            return RedirectToAction("login", "account");
        }

        [Route("{action=signup}")]
        public ActionResult signup(string returnUrl = "")
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [Route("/signup-post")]
        [HttpPost]
        public ActionResult signup_post(User user, string returnUrl = "")
        {
            user.role = Role.customer;
            db.tbl_user.Add(user);
            db.SaveChanges();
            Session["user"] = user;
            Session["shoppingCartCount"] = new HomeController().GetShoppingCartItemsCount(user);
            if (string.IsNullOrEmpty(returnUrl))
            {
                ViewBag.msg = "Registration Successfully !";
                return RedirectToAction("dashboard", "user");
            }
            else
                return Redirect(returnUrl);
        }
    }
}
