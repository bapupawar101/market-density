﻿using AttributeRouting.Web.Mvc;
using MarketDensity_Web_Application.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MarketDensity_Web_Application.Controllers
{
    public class HomeController : Controller
    {
        MarketDensity_Db_Contetxt db = new MarketDensity_Db_Contetxt();

        public ActionResult Index()
        {
            ViewBag.categories = db.tbl_category.ToList();
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult faqs()
        {
            return View();
        }

        [Route("{action=contact}")]
        public ActionResult contact(string @ref = "", string code = "", int reportid = 0)
        {
            Contact model = new Contact() { reportCode = code, request_category = @ref };
            if (reportid != 0)
            {
                ViewBag.report = db.tbl_report.Find(reportid);
            }
            return View(model);
        }

        [Route("contact-post")]
        [HttpPost]
        public ActionResult contact_post(Contact record)
        {
            TempData["msg"] = "Thank you, We will get back to you soon!";
            db.tbl_contactus.Add(record);
            db.SaveChanges();

            return RedirectToAction("contact", "home");
        }

        [Route("/report-categories")]
        public ActionResult report_categories(int cat = 0, int publisher = 0, int pageIndex = 1, string key = "")
        {
            ViewBag.key = key;
            int take = 10;
            int skip = take * (pageIndex - 1);
            var listOfReports = new List<Report_Master>();
            if (cat == 0 && publisher == 0)
            {
                if (string.IsNullOrEmpty(key))
                    listOfReports = db.tbl_report.ToList();
                else
                    listOfReports = db.tbl_report.Where(p => (key.ToLower().Trim().Equals(p.code) || p.title.Contains(key.Trim()) || p.description.Contains(key))).ToList();
            }
            else if (cat != 0)
            {
                var category = db.tbl_category.Find(cat);
                ViewBag.category = category;
                listOfReports = db.tbl_report.Where(p => p.category.Trim().ToLower().Equals(category.name.Trim().ToLower())).ToList();
            }
            else if (publisher != 0)
            {
                var pub = db.tbl_publisher.Find(publisher);
                ViewBag.publisher = pub;
                listOfReports = db.tbl_report.Where(p => p.publisher.Trim().ToLower().Equals(pub.displayName.Trim().ToLower())).ToList();
            }

            ViewBag.totalCount = listOfReports.Count();
            ViewBag.pager = new Pager(ViewBag.totalCount, pageIndex);

            return View(listOfReports);
        }

        public ActionResult report_details(int id, string code)
        {
            var report = db.tbl_report.Find(id);
            return View(report);
        }

        public int addToCart(string reportCode, LicenseType lType)
        {
            var user = GetUser();
            if (user == null)
                return -1;

            bool isduplicate = db.tbl_cart.Where(p => p.reportId.Equals(reportCode) && p.userId==user.id).Count() > 0;
            if (isduplicate)
                return -2;

            var cart = new Cart() { licenseType = lType, reportId = reportCode, userId = user.id };
            db.tbl_cart.Add(cart);
            db.SaveChanges();
            var count = GetShoppingCartItemsCount();

            updateshoppingcartcount();
            return count;
        }

        public ActionResult removeItemFromCart(int id)
        {
            var item = db.tbl_cart.Find(id);
            db.tbl_cart.Remove(item);
            db.SaveChanges();
            updateshoppingcartcount();
            return RedirectToAction("checkout", "payment");
        }

        public int GetShoppingCartItemsCount(User dbUser = null)
        {
            var user = dbUser;
            if (user == null)
                user=GetUser();
            return db.tbl_cart.Where(p => p.userId == user.id).Count();
        }

        public void updateshoppingcartcount()
        {
            Session["shoppingCartCount"] = GetShoppingCartItemsCount();
        }

        public User GetUser()
        {
            return Session["user"] as MarketDensity_Web_Application.Models.User;
        }

        //[HttpPost]
        //public ActionResult contactus(Contact contact)
        //{
        //    db.tbl_contactus.Add(contact);
        //    db.SaveChanges();
        //    TempData["msg"] = "Thank you for contacting us. One of our representatives will be in contact with you shortly regarding your inquiry.";
        //    return RedirectToAction("contactus", "home");
        //}

        [Route("{action=publisher}")]
        public ActionResult Publisher()
        {
            var publishers = db.tbl_publisher.ToList();
            return View(publishers);
        }

        [Route("{action=categories}")]
        public ActionResult categories()
        {
            var publishers = db.tbl_category.ToList();
            return View(publishers);
        }

    }
}
