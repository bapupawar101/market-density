﻿using MarketDensity_Web_Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarketDensity_Web_Application.Helper
{
    public static class CommonUtils
    {
        public static string GetLicenseTypeName(LicenseType type)
        {
            string name = "";
            if (type == MarketDensity_Web_Application.Models.LicenseType.single)
            {
                name = "Single User";
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.multi)
            {
                name = "Multi User";
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.online_only)
            {
                name = "Online Only User";
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.data_pack)
            {
                name = "Data Pack";
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.enterprise)
            {
                name = "Enterprise User";
            }

            return name;
        }

        public static double GetReportPrice(Report_Master report, LicenseType type)
        {
            double price = 0;
            if (type == MarketDensity_Web_Application.Models.LicenseType.single)
            {
                price = report.price_single_user;
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.multi)
            {
                price = report.price_multi_user;
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.online_only)
            {
                price = report.price_online_only;
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.data_pack)
            {
                price = report.price_data_pack;
            }
            else if (type == MarketDensity_Web_Application.Models.LicenseType.enterprise)
            {
                price = report.price_enterprise_user;
            }

            return price;
        }
    }
}